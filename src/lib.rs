//! # Apub: utilities for building activitypub servers
//!
//! ## Delivery
//!
//! The [`Deliver`] trait defines how to deliver objects to remote servers. There are three
//! implementations of `Deliver` provided by this crate: one backed by Reqwest, one backed by Awc,
//! and one backed by Background Jobs, with the HTTP portion backed by another Deliver
//! implementation.
//!
//! For more information, see the [`clients`] module
//!
//! ## Fetching
//!
//! The [`Repo`] trait defines how to fetch an Object from somewhere. This is generic enough to be
//! used for fetching from a Database as well as fetching from a remote server. There are no
//! database implementations provided by default, implementing `Repo` is simple enough with the help
//! of [`async_trait`](https://docs.rs/async-trait/0.1.51/async_trait/). There are two HTTP
//! implementations of `Repo` provided by apub, one backed by Reqwest and one backed by Apub.
//!
//! For more information, see the [`clients`] module
//!
//! ## Accepting
//!
//! The [`Ingest`] trait is the main point for accepting activities into your system. `Ingest` is
//! generic over the type it ingests. This allows for a single type to accept multiple kinds of
//! activities with different implementations. It may be worthwile to implement
//! `Ingest<DeleteActivity>` differently from `Ingest<UndoActivity>`. Further, implementations can
//! implement `Ingest<A> where A: SomeTrait`. Creating generic `Ingest` implementations can enable
//! better code re-use between kinds of activities.
//!
//! See the [`ingest`] module for more types and traits to help with accepting activities
//!
//! ## Request Utilities
//!
//! When you're building an application that frequently makes HTTP Requests, you may want to
//! preemptively stop a request from continuing. For this behavior, apub provides the [`Session`]
//! trait. When a request is made via Awc or Reqwest's Repo or Deliver implementations, a check to
//! the current `Session` is made to see if the given request should procede, and when the request
//! finishes, the current session is notified of whether the request completed succesfully or
//! failed. Two Session types are provided by default: [`BreakerSession`] and
//! [`RequestCountSession`]. `BreakerSession` is designed to be shared between all requests the
//! application makes, and will stop requests to domains with too many consecutive failures for a
//! configured duration. `RequestCountSession` will keep track of how many requests a client makes
//! in total, and prevents future requests if a configured limit is exceeded. A new
//! `RequestCountSession` should be instantiated for each `Ingest`
//!
//!
//! ## Request Validation
//!
//! Much like transportation, apub provides two implementations for cryptography, one backed by
//! OpenSSL and one backed by the Rustcrypto libraries. When using the provided HTTP Repo
//! implementations, HTTP SIgnatures and HTTP Digests will automatically be applied to all requests.
//!
//! For dealing with Public and Private keys, two additional traits are provided: [`PublicKeyRepo`]
//! and [`PrivateKeyRepo`]. These traits define an API for storing and retrieving keys that server
//! ingegrations can rely on for signing and validating requests.
//!
//! For more information, see the [`cryptography`] and [`activitypub::keys`] modules.
//!
//! ## ActivityPub Impelementation
//!
//! Apub provides basic traits for dealing with a few kinds of activitypub Objects. These traits are
//! far from a perfect representation of the activitypub spec, and are instead meant to ease dealing
//! with common types.
//!
//! In addition, apub provides simple concrete implementations for an Object, and Actor, an
//! Activity, and a Public Key. These implementations may be too basic to be of much help, but are
//! included to aide in prototyping. The `SimplePublicKey` type is used in the [`PublicKeyRepo`]
//! api.
//!
//! For more information, see the [`activitypub`] module.
//!
//! ## Feature Flags
//!
//! | Feature              | Description                                          | default |
//! | -------------------- | ---------------------------------------------------- | ------- |
//! | full                 | Enable every feature                                 | false   |
//! | with-actix-web       | Enable apub's actix-web integration                  | false   |
//! | with-awc             | Enable apub's awc Repo and Deliver                   | false   |
//! | with-background-jobs | Enable apub's background_jobs delivery integration   | false   |
//! | with-openssl         | Enable HTTP Signatures with OpenSSL                  | false   |
//! | with-reqwest         | Enable apub's reqwest Repo and Deliver               | false   |
//! | with-rustcrypto      | Enable HTTP Signatures with rustcrypto               | false   |
//! | utils                | Enable helper types and traits not strictly required | true    |
//!
//! ## Examples
//! See the [examples directory](https://git.asonix.dog/asonix/apub/src/branch/main/examples) for
//! concrete implementations.
//!
//! [`BreakerSession`]: session::BreakerSession
//! [`RequestCountSession`]: session::RequestCountSession
//! [`PublicKeyRepo`]: activitypub::keys::PublicKeyRepo
//! [`PrivateKeyRepo`]: activitypub::keys::PrivateKeyRepo
pub use apub_core::{
    deliver::{Deliver, Deliverable},
    ingest::Ingest,
    object_id::ObjectId,
    repo::{Dereference, Repo, RepoFactory},
    session::Session,
};

pub mod clients {
    //! Ready-made Implementations of Repo and Deliver

    #[cfg(feature = "apub-awc")]
    pub use apub_awc::AwcClient;

    #[cfg(feature = "apub-reqwest")]
    pub use apub_reqwest::ReqwestClient;

    #[cfg(feature = "apub-background-jobs")]
    pub use apub_background_jobs::JobClient;

    #[cfg(feature = "apub-awc")]
    pub mod awc {
        pub use apub_awc::{
            AwcError, InvalidHeaderValue, PrepareSignError, SignatureConfig, SignatureError,
        };
    }

    #[cfg(feature = "apub-reqwest")]
    pub mod reqwest {
        pub use apub_reqwest::{ReqwestError, SignatureConfig, SignatureError};
    }

    #[cfg(feature = "apub-background-jobs")]
    pub mod background_jobs {
        pub use apub_background_jobs::{ClientFactory, DeliverJob, EnqueueError};
    }
}

pub mod ingest {
    //! Utilities for ingesting activities

    pub use apub_core::ingest::{Authority, IngestFactory};

    #[cfg(feature = "apub-ingest")]
    pub use apub_ingest::{validate_authority, validate_hosts, validate_inbox};

    #[cfg(feature = "apub-ingest")]
    pub mod validators {
        //! Pre-defined validators for Ingest implementations

        pub use apub_ingest::{
            AuthorityError, HostError, InboxError, InboxType, ValidateAuthority, ValidateHosts,
            ValidateInbox,
        };
    }
}

#[cfg(feature = "apub-actix-web")]
pub mod servers {
    //! apub integration with http servers

    pub mod actix_web {
        //! actix-web integration
        pub use apub_actix_web::{inbox, serve_objects, Config, SignatureConfig, VerifyError};
    }
}

pub mod activitypub {
    //! Types and traits for building activitypub servers

    pub use apub_core::activitypub::{Activity, Actor, DeliverableObject, Object, PublicKey};

    pub mod extensions {
        //! Extensions of the activitypub traits, with dereferencing via FullRepo

        pub use apub_core::activitypub_ext::{dereference, ActivityExt, ActorExt, Out};
    }

    pub mod simple {
        //! Basic concrete types implementing activitypub traits

        #[cfg(feature = "apub-simple-activitypub")]
        pub use apub_simple_activitypub::{
            SimpleActivity, SimpleActor, SimpleEndpoints, SimpleObject,
        };
    }

    pub mod keys {
        //! Helpers for dealing with public and private keys

        pub use apub_privatekey::{KeyId, PrivateKeyRepo, PrivateKeyRepoFactory};

        #[cfg(feature = "apub-publickey")]
        pub use apub_publickey::{PublicKeyRepo, SimplePublicKey};

        pub mod publickey {
            //! More types and traits for dealing with public keys

            #[cfg(feature = "apub-publickey")]
            pub use apub_publickey::{PublicKeyClient, PublicKeyError, PublicKeyRepoFactory};
        }
    }
}

pub mod session {
    //! Implementations and extra traits for Sessions

    pub use apub_core::session::{RequestCountSession, SessionError, SessionFactory};

    #[cfg(feature = "apub-breaker-session")]
    pub use apub_breaker_session::BreakerSession;
}

pub mod cryptography {
    //! Cryptography implementations for HTTP Signatures

    pub use apub_core::{
        digest::DigestFactory,
        signature::{PrivateKey, VerifyFactory},
    };

    #[cfg(feature = "apub-openssl")]
    pub use apub_openssl::OpenSsl;

    #[cfg(feature = "apub-rustcrypto")]
    pub use apub_rustcrypto::Rustcrypto;

    pub mod digest {
        //! More traits for dealing with digests

        pub use apub_core::digest::{Digest, DigestBuilder};
    }

    #[cfg(feature = "apub-openssl")]
    pub mod openssl {
        //! More openssl types for signing and verification

        pub use apub_openssl::{OpenSslDigest, OpenSslSigner, OpenSslVerifier};
    }

    #[cfg(feature = "apub-rustcrypto")]
    pub mod rustcrypto {
        //! More rustcrypto types for signing and verification

        pub use apub_rustcrypto::{RsaSigner, RsaVerifier, RustcryptoError, Sha256Digest};
    }

    pub mod signature {
        //! More traits for signing and verification
        pub use apub_core::signature::{PrivateKeyBuilder, Sign, Verify, VerifyBuilder};
    }
}
