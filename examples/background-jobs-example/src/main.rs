use apub::{
    clients::{
        awc::SignatureConfig,
        background_jobs::{ClientFactory, DeliverJob},
        AwcClient, JobClient,
    },
    cryptography::Rustcrypto,
    session::{BreakerSession, RequestCountSession, SessionFactory},
    Deliverable,
};
use background_jobs::{memory_storage::Storage, WorkerConfig};
use example_types::{object_id, Note, NoteType};
use rsa::RsaPrivateKey;
use std::time::Duration;
use tracing::subscriber::set_global_default;
use tracing_error::ErrorLayer;
use tracing_log::LogTracer;
use tracing_subscriber::{fmt::format::FmtSpan, layer::SubscriberExt, EnvFilter, Registry};

pub fn init_tracing() -> Result<(), Box<dyn std::error::Error>> {
    LogTracer::init()?;

    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));
    let format_layer = tracing_subscriber::fmt::layer()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .pretty();

    let subscriber = Registry::default()
        .with(env_filter)
        .with(format_layer)
        .with(ErrorLayer::default());

    set_global_default(subscriber)?;

    Ok(())
}

#[derive(Clone)]
struct State {
    config: SignatureConfig,
    breakers: BreakerSession,
    client: awc::Client,
}

impl ClientFactory for State {
    type Crypto = Rustcrypto;
    type Client = AwcClient<Rustcrypto>;

    fn build_client(&self, crypto: &Self::Crypto) -> Self::Client {
        AwcClient::new(self.client.clone(), self.config.clone(), crypto.clone())
    }
}

impl SessionFactory for State {
    type Session = (RequestCountSession, BreakerSession);

    fn build_session(&self) -> Self::Session {
        (RequestCountSession::max(25), self.breakers.clone())
    }
}

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    init_tracing()?;

    let config = SignatureConfig::default();
    let breakers = BreakerSession::limit(3, Duration::from_secs(60 * 60));

    let storage = Storage::new();

    let manager = WorkerConfig::new_managed(storage, move |_| State {
        config: config.clone(),
        breakers: breakers.clone(),
        client: awc::Client::new(),
    })
    .register::<DeliverJob<State, Rustcrypto>>()
    .start();

    let private_key = RsaPrivateKey::new(&mut rand::thread_rng(), 1024)?;
    let crypto = Rustcrypto::new("key-id".to_string(), private_key);

    let inbox = "https://masto.asonix.dog/inbox".parse()?;
    let note = Note {
        id: object_id("https://masto.asonix.dog/blah/blah".parse()?),
        kind: NoteType::Note,
        content: String::from("hi"),
    };
    note.deliver(
        &inbox,
        &JobClient::<State>::new(crypto, manager.queue_handle().clone()),
        (),
    )
    .await?;

    actix_rt::signal::ctrl_c().await?;

    drop(manager);

    Ok(())
}
