use apub::{
    clients::{awc::SignatureConfig, AwcClient},
    cryptography::Rustcrypto,
    session::{BreakerSession, RequestCountSession},
    Deliverable,
};
use example_types::{object_id, NoteType, ObjectId};
use rsa::RsaPrivateKey;
use std::time::Duration;

#[actix_rt::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let private_key = RsaPrivateKey::new(&mut rand::thread_rng(), 1024)?;
    let crypto = Rustcrypto::new("key-id".to_string(), private_key);
    let config = SignatureConfig::default();

    let mut breakers = BreakerSession::limit(10, Duration::from_secs(60 * 60));

    let mut session = (RequestCountSession::max(30), &mut breakers);

    let client = awc::Client::new();

    let awc_client = AwcClient::new(client, config, &crypto);

    let id: ObjectId<NoteType> =
        object_id("https://masto.asonix.dog/users/asonix/statuses/107289461429162429".parse()?);

    if let Some(note) = id.dereference(&mut session, &awc_client).await? {
        println!("id: {}, content: {}", note.id, note.content);

        let inbox = "https://masto.asonix.dog/inbox".parse()?;
        note.deliver(&inbox, &awc_client, &mut session).await?;
    }

    Ok(())
}
