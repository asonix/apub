use apub::{
    activitypub::{Activity, DeliverableObject, Object},
    Deliverable, Dereference, Repo, Session,
};
use std::{
    fmt::Display,
    ops::{Deref, DerefMut},
};
use url::Url;

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub enum ActivityType {
    Create,
    Update,
    Delete,
    Undo,
    Follow,
    Accept,
    Reject,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct AcceptedActivity {
    id: ObjectId<ActivityType>,

    #[serde(rename = "type")]
    kind: ActivityType,

    to: Vec<Url>,

    cc: Vec<Url>,

    actor: Url,

    object: Url,
}

impl Object for AcceptedActivity {
    type Kind = ActivityType;

    fn id(&self) -> &Url {
        &self.id
    }

    fn kind(&self) -> &Self::Kind {
        &self.kind
    }
}

impl DeliverableObject for AcceptedActivity {
    fn to(&self) -> &[Url] {
        &self.to
    }

    fn cc(&self) -> &[Url] {
        &self.cc
    }
}

impl Activity for AcceptedActivity {
    fn actor_id(&self) -> &Url {
        &self.actor
    }

    fn object_id(&self) -> &Url {
        &self.object
    }
}

impl AcceptedActivity {
    pub fn id(&self) -> &ObjectId<ActivityType> {
        &self.id
    }
}

impl Dereference for ObjectId<ActivityType> {
    type Output = AcceptedActivity;

    fn url(&self) -> &Url {
        &self.0
    }
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(transparent)]
pub struct ObjectId<Kind>(apub::ObjectId<'static, Kind>);

pub fn object_id<Kind>(id: Url) -> ObjectId<Kind>
where
    ObjectId<Kind>: Dereference,
{
    ObjectId(apub::ObjectId::new_owned(id))
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub enum NoteType {
    Note,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct Note {
    pub id: ObjectId<NoteType>,

    #[serde(rename = "type")]
    pub kind: NoteType,

    pub content: String,
}

impl<Kind> Deref for ObjectId<Kind> {
    type Target = Url;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<Kind> DerefMut for ObjectId<Kind> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl<Kind> Display for ObjectId<Kind> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl<Kind> From<Url> for ObjectId<Kind>
where
    Self: Dereference,
{
    fn from(url: Url) -> Self {
        object_id::<Kind>(url)
    }
}

impl Dereference for ObjectId<NoteType> {
    type Output = Note;

    fn url(&self) -> &Url {
        self
    }
}

impl<Kind> ObjectId<Kind>
where
    Self: Dereference,
{
    pub async fn dereference<R, S>(
        &self,
        session: S,
        repo: &R,
    ) -> Result<Option<<Self as Dereference>::Output>, Box<dyn std::error::Error>>
    where
        S: Session,
        R: Repo,
        R::Error: std::error::Error + 'static,
    {
        Ok(repo.fetch(self, session).await?)
    }
}

impl Deliverable for Note {}
