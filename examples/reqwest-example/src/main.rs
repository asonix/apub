use apub::{
    clients::{reqwest::SignatureConfig, ReqwestClient},
    cryptography::OpenSsl,
    session::{BreakerSession, RequestCountSession},
    Deliverable,
};
use example_types::{object_id, NoteType, ObjectId};
use openssl::{pkey::PKey, rsa::Rsa};
use reqwest_middleware::ClientBuilder;
use std::time::Duration;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let private_key = PKey::from_rsa(Rsa::generate(1024)?)?;
    let crypto = OpenSsl::new("key-id".to_string(), private_key);
    let config = SignatureConfig::default();

    let mut breakers = BreakerSession::limit(10, Duration::from_secs(60 * 60));

    let mut session = (RequestCountSession::max(30), &mut breakers);
    let client = ClientBuilder::new(reqwest::Client::new()).build();

    let reqwest_client = ReqwestClient::new(client, config, &crypto);

    let id: ObjectId<NoteType> =
        object_id("https://masto.asonix.dog/users/asonix/statuses/107289461429162429".parse()?);

    if let Some(note) = id.dereference(&mut session, &reqwest_client).await? {
        println!("id: {}, content: {}", note.id, note.content);

        let inbox = "https://masto.asonix.dog/inbox".parse()?;
        note.deliver(&inbox, &reqwest_client, &mut session).await?;
    }

    Ok(())
}
