use actix_web::{middleware::Logger, web, App, HttpServer, ResponseError};
use apub::{
    activitypub::{
        keys::{
            publickey::{PublicKeyError, PublicKeyRepoFactory},
            KeyId, PrivateKeyRepo, PrivateKeyRepoFactory, PublicKeyRepo, SimplePublicKey,
        },
        simple::{SimpleActor, SimpleEndpoints},
    },
    clients::{
        awc::{AwcError, SignatureConfig as AwcSignatureConfig},
        AwcClient,
    },
    cryptography::{
        rustcrypto::{RsaVerifier, RustcryptoError, Sha256Digest},
        signature::PrivateKeyBuilder,
        DigestFactory, PrivateKey, Rustcrypto, VerifyFactory,
    },
    ingest::validators::{AuthorityError, HostError, InboxError},
    servers::actix_web::{
        inbox, serve_objects, Config, SignatureConfig as ActixWebSignatureConfig, VerifyError,
    },
    session::{BreakerSession, RequestCountSession, SessionFactory},
    Dereference, Repo, RepoFactory, Session,
};
use dashmap::DashMap;
use example_types::{ActivityType, ObjectId};
use rsa::{
    pkcs8::{ToPrivateKey, ToPublicKey},
    RsaPrivateKey,
};
use serde_json::Map;
use std::{sync::Arc, time::Duration};
use url::{Host, Url};

mod ingest;

use ingest::{ActivityIngester, ActorId};

#[derive(Clone, Default)]
struct MemoryRepo {
    inner: Arc<DashMap<Url, serde_json::Value>>,
}

#[derive(Clone, Default)]
struct PrivateKeyStore {
    inner: Arc<DashMap<Url, (KeyId, String)>>,
}

#[derive(Clone)]
struct RequestVerifier {
    repo: MemoryRepo,
    private_key_store: PrivateKeyStore,
    session: BreakerSession,
    client: awc::Client,
    signature_config: AwcSignatureConfig,
}

#[derive(Debug, thiserror::Error)]
enum ServerError {
    #[error(transparent)]
    Json(#[from] serde_json::Error),

    #[error(transparent)]
    Verify(#[from] VerifyError),

    #[error(transparent)]
    Rustcrypto(#[from] RustcryptoError),

    #[error("Public key owner doesn't match actor ID")]
    OwnerMismatch,

    #[error(transparent)]
    Awc(#[from] AwcError<RustcryptoError>),

    #[error(transparent)]
    Key(#[from] KeyError),

    #[error(transparent)]
    Authority(#[from] AuthorityError),

    #[error(transparent)]
    Inbox(#[from] InboxError),

    #[error(transparent)]
    Hosts(#[from] HostError),
}

impl From<PublicKeyError<serde_json::Error, AwcError<RustcryptoError>>> for ServerError {
    fn from(e: PublicKeyError<serde_json::Error, AwcError<RustcryptoError>>) -> Self {
        match e {
            PublicKeyError::RemoteRepo(awc_error) => awc_error.into(),
            PublicKeyError::PublicKeyRepo(json_error) => json_error.into(),
            PublicKeyError::OwnerMismatch => Self::OwnerMismatch,
        }
    }
}

impl MemoryRepo {
    fn insert<D>(&self, id: D, item: &D::Output) -> Result<(), serde_json::Error>
    where
        D: Dereference,
        D::Output: serde::ser::Serialize,
    {
        let value = serde_json::to_value(item)?;
        self.inner.insert(id.url().clone(), value);
        Ok(())
    }
}

impl ResponseError for ServerError {}

#[async_trait::async_trait(?Send)]
impl Repo for MemoryRepo {
    type Error = serde_json::Error;

    async fn fetch<D: Dereference, S: Session>(
        &self,
        id: D,
        _session: S,
    ) -> Result<Option<D::Output>, Self::Error> {
        if let Some(obj_ref) = self.inner.get(id.url()) {
            serde_json::from_value(obj_ref.clone()).map(Some)
        } else {
            Ok(None)
        }
    }
}

#[async_trait::async_trait(?Send)]
impl PublicKeyRepo for MemoryRepo {
    type Error = serde_json::Error;

    async fn fetch(&self, key_id: &KeyId) -> Result<Option<SimplePublicKey>, Self::Error> {
        if let Some(obj_ref) = self.inner.get(key_id) {
            return serde_json::from_value(obj_ref.clone()).map(Some);
        }

        Ok(None)
    }

    async fn store(&self, public_key: &SimplePublicKey) -> Result<(), Self::Error> {
        let value = serde_json::to_value(public_key)?;

        self.inner.insert(public_key.id.as_ref().clone(), value);

        Ok(())
    }
}

#[derive(Debug)]
enum KeyError {
    Missing,
    Url,
    Rustcrypto(RustcryptoError),
}

impl std::fmt::Display for KeyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Missing => {
                write!(f, "Key is missing")
            }
            Self::Url => {
                write!(f, "Couldn't parse key id")
            }
            Self::Rustcrypto(rustcrypto_err) => std::fmt::Display::fmt(rustcrypto_err, f),
        }
    }
}

impl std::error::Error for KeyError {}

#[async_trait::async_trait(?Send)]
impl PrivateKeyRepo for PrivateKeyStore {
    type PrivateKey = Rustcrypto;
    type Error = KeyError;

    async fn fetch(&self, actor_id: &Url) -> Result<Self::PrivateKey, Self::Error> {
        if let Some(tup_ref) = self.inner.get(actor_id) {
            return Rustcrypto::build(tup_ref.0.to_string(), &tup_ref.1)
                .map_err(KeyError::Rustcrypto);
        }

        Err(KeyError::Missing)
    }

    async fn store(&self, actor_id: Url, key: &Self::PrivateKey) -> Result<(), Self::Error> {
        let key_id = key.key_id().parse().map_err(|_| KeyError::Url)?;
        let private_key_pem = key.private_key_pem().map_err(KeyError::Rustcrypto)?;

        self.inner.insert(actor_id, (key_id, private_key_pem));
        Ok(())
    }
}

impl PrivateKeyRepoFactory for RequestVerifier {
    type PrivateKeyRepo = PrivateKeyStore;

    fn build_private_key_repo(&self) -> Self::PrivateKeyRepo {
        self.private_key_store.clone()
    }
}

impl RepoFactory for MemoryRepo {
    type Crypto = ();
    type Repo = MemoryRepo;

    fn build_repo(&self, _: Self::Crypto) -> Self::Repo {
        self.clone()
    }
}

impl VerifyFactory for RequestVerifier {
    type Verify = RsaVerifier;
}

impl RepoFactory for RequestVerifier {
    type Crypto = Rustcrypto;
    type Repo = AwcClient<Rustcrypto>;

    fn build_repo(&self, crypto: Self::Crypto) -> Self::Repo {
        AwcClient::new(self.client.clone(), self.signature_config.clone(), crypto)
    }
}

impl PublicKeyRepoFactory for RequestVerifier {
    type PublicKeyRepo = MemoryRepo;

    fn public_key_repo(&self) -> Self::PublicKeyRepo {
        self.repo.clone()
    }
}

impl SessionFactory for RequestVerifier {
    type Session = (RequestCountSession, BreakerSession);

    fn build_session(&self) -> Self::Session {
        (RequestCountSession::max(25), self.session.clone())
    }
}

impl DigestFactory for RequestVerifier {
    type Digest = Sha256Digest;
}

#[actix_web::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    if std::env::var("RUST_LOG").is_ok() {
        env_logger::builder().init()
    } else {
        env_logger::Builder::new()
            .filter_level(log::LevelFilter::Info)
            .init();
    };

    let repo = MemoryRepo::default();
    let private_key_store = PrivateKeyStore::default();
    let session = BreakerSession::limit(10, Duration::from_secs(60 * 60));
    let signature_config = ActixWebSignatureConfig::default();

    let actor_id: Url = "http://localhost:8008/actors/localhost".parse()?;
    let config = Config {
        local_host: Host::Domain("localhost".to_string()),
        local_port: Some(8008),
        scheme: "http://".to_string(),
        server_actor_id: actor_id.clone(),
    };
    let awc_signature_config = AwcSignatureConfig::default();

    let private_key = RsaPrivateKey::new(&mut rand::thread_rng(), 1024)?;
    let private_key_pem = private_key.to_pkcs8_pem()?;
    let key_id: KeyId = "http://localhost:8008/actors/localhost#main-key".parse()?;

    let public_key = private_key.to_public_key();
    let public_key_pem = public_key.to_public_key_pem()?;

    let public_key = SimplePublicKey {
        id: key_id.clone(),
        owner: actor_id.clone(),
        public_key_pem,
    };

    let server_actor = SimpleActor {
        id: actor_id.clone(),
        kind: "Application".to_string(),
        preferred_username: "localhost".to_string(),
        inbox: "http://localhost:8008/inbox".parse()?,
        outbox: "http://localhost:8008/outbox".parse()?,
        name: Some("Demo Server".to_string()),
        public_key,
        endpoints: Some(SimpleEndpoints {
            shared_inbox: Some("http://localhost:8008/inbox".parse()?),
        }),
        rest: Map::default(),
    };

    repo.insert(ActorId(actor_id.clone()), &server_actor)?;

    let key = Rustcrypto::build(key_id.to_string(), &private_key_pem)?;

    private_key_store.store(actor_id, &key).await?;

    HttpServer::new(move || {
        let client = awc::Client::new();

        let verifier = RequestVerifier {
            repo: repo.clone(),
            private_key_store: private_key_store.clone(),
            session: session.clone(),
            client: client.clone(),
            signature_config: awc_signature_config.clone(),
        };
        let ingester = ActivityIngester {
            repo: repo.clone(),
            private_key_store: private_key_store.clone(),
            session: session.clone(),
            client,
            signature_config: awc_signature_config.clone(),
            config: config.clone(),
        };

        App::new()
            .wrap(Logger::default())
            .service(
                web::scope("/inbox").configure(inbox::<_, _, _, ServerError>(
                    config.clone(),
                    ingester,
                    verifier.clone(),
                    signature_config.clone(),
                    false,
                )),
            )
            .service(web::scope("/activities").configure(serve_objects::<
                ObjectId<ActivityType>,
                _,
                _,
                ServerError,
            >(
                config.clone(),
                repo.clone(),
                verifier.clone(),
                signature_config.clone(),
                false,
            )))
            .service(
                web::scope("/actors").configure(serve_objects::<ActorId, _, _, ServerError>(
                    config.clone(),
                    repo.clone(),
                    verifier,
                    signature_config.clone(),
                    false,
                )),
            )
    })
    .bind("127.0.0.1:8008")?
    .run()
    .await?;
    Ok(())
}
