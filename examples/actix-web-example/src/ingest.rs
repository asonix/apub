use crate::{MemoryRepo, PrivateKeyStore, ServerError};
use actix_web::FromRequest;
use apub::{
    activitypub::{keys::PrivateKeyRepoFactory, simple::SimpleActor},
    clients::{awc::SignatureConfig as AwcSignatureConfig, AwcClient},
    cryptography::Rustcrypto,
    ingest::{
        validate_authority, validate_hosts, validate_inbox,
        validators::{InboxType, ValidateAuthority, ValidateHosts, ValidateInbox},
        Authority, IngestFactory,
    },
    servers::actix_web::Config,
    session::{BreakerSession, RequestCountSession, SessionFactory},
    Dereference, Ingest, Repo, RepoFactory, Session,
};
use example_types::AcceptedActivity;
use std::future::{ready, Ready};
use url::Url;

#[derive(Clone)]
pub(super) struct ActivityIngester {
    pub(super) repo: MemoryRepo,
    pub(super) private_key_store: PrivateKeyStore,
    pub(super) session: BreakerSession,
    pub(super) client: awc::Client,
    pub(super) signature_config: AwcSignatureConfig,
    pub(super) config: Config,
}

pub(super) struct InboxActor(pub(super) Url);

pub(super) struct ActorId(pub(super) Url);

impl PrivateKeyRepoFactory for ActivityIngester {
    type PrivateKeyRepo = PrivateKeyStore;

    fn build_private_key_repo(&self) -> Self::PrivateKeyRepo {
        self.private_key_store.clone()
    }
}

impl IngestFactory<AcceptedActivity> for ActivityIngester {
    type Ingest = ValidateHosts<ValidateInbox<ValidateAuthority<ActivityIngester>>>;

    fn build_ingest(&self) -> Self::Ingest {
        validate_hosts(validate_inbox(validate_authority(self.clone())))
    }
}

#[async_trait::async_trait(?Send)]
impl Ingest<AcceptedActivity> for ActivityIngester {
    type Local = MemoryRepo;
    type Error = ServerError;
    type ActorId = InboxActor;

    fn local_repo(&self) -> &Self::Local {
        &self.repo
    }

    fn is_local(&self, url: &Url) -> bool {
        self.config.is_local(url)
    }

    async fn ingest<R: Repo, S: Session>(
        &self,
        _authority: Authority,
        _actor_id: Self::ActorId,
        activity: &AcceptedActivity,
        _remote_repo: R,
        _session: S,
    ) -> Result<(), Self::Error>
    where
        Self::Error: From<R::Error>,
    {
        Ok(self.repo.insert(activity.id().clone(), activity)?)
    }
}

impl RepoFactory for ActivityIngester {
    type Crypto = Rustcrypto;
    type Repo = AwcClient<Rustcrypto>;

    fn build_repo(&self, crypto: Self::Crypto) -> Self::Repo {
        AwcClient::new(self.client.clone(), self.signature_config.clone(), crypto)
    }
}

impl SessionFactory for ActivityIngester {
    type Session = (RequestCountSession, BreakerSession);

    fn build_session(&self) -> Self::Session {
        (RequestCountSession::max(25), self.session.clone())
    }
}

impl FromRequest for InboxActor {
    type Error = actix_web::Error;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(_: &actix_web::HttpRequest, _: &mut actix_web::dev::Payload) -> Self::Future {
        ready(Ok(InboxActor(
            Url::parse("http://localhost:8008/actors/localhost").unwrap(),
        )))
    }
}

impl InboxType for InboxActor {
    fn is_shared(&self) -> bool {
        true
    }
}

impl AsRef<Url> for InboxActor {
    fn as_ref(&self) -> &Url {
        &self.0
    }
}

impl Dereference for ActorId {
    type Output = SimpleActor;
    fn url(&self) -> &Url {
        &self.0
    }
}

impl From<Url> for ActorId {
    fn from(url: Url) -> Self {
        ActorId(url)
    }
}
