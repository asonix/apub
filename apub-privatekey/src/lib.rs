use apub_core::signature::PrivateKeyBuilder;
use std::{
    ops::{Deref, DerefMut},
    rc::Rc,
    str::FromStr,
    sync::Arc,
};
use url::Url;

#[async_trait::async_trait(?Send)]
pub trait PrivateKeyRepo {
    type PrivateKey: PrivateKeyBuilder;
    type Error;

    async fn store(&self, actor_id: Url, key: &Self::PrivateKey) -> Result<(), Self::Error>;

    async fn fetch(&self, actor_id: &Url) -> Result<Self::PrivateKey, Self::Error>;
}

pub trait PrivateKeyRepoFactory {
    type PrivateKeyRepo: PrivateKeyRepo;

    fn build_private_key_repo(&self) -> Self::PrivateKeyRepo;
}

#[derive(
    Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Deserialize, serde::Serialize,
)]
#[serde(transparent)]
pub struct KeyId(Url);

impl KeyId {
    pub fn into_inner(this: Self) -> Url {
        this.0
    }
}

impl FromStr for KeyId {
    type Err = <Url as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(KeyId(Url::from_str(s)?))
    }
}

impl Deref for KeyId {
    type Target = Url;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for KeyId {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl AsRef<Url> for KeyId {
    fn as_ref(&self) -> &Url {
        &self.0
    }
}

impl AsMut<Url> for KeyId {
    fn as_mut(&mut self) -> &mut Url {
        &mut self.0
    }
}

impl From<Url> for KeyId {
    fn from(url: Url) -> Self {
        KeyId(url)
    }
}

#[async_trait::async_trait(?Send)]
impl<'a, T> PrivateKeyRepo for &'a T
where
    T: PrivateKeyRepo,
{
    type PrivateKey = T::PrivateKey;
    type Error = T::Error;

    async fn store(&self, actor_id: Url, key: &Self::PrivateKey) -> Result<(), Self::Error> {
        T::store(self, actor_id, key).await
    }

    async fn fetch(&self, actor_id: &Url) -> Result<Self::PrivateKey, Self::Error> {
        T::fetch(self, actor_id).await
    }
}

#[async_trait::async_trait(?Send)]
impl<'a, T> PrivateKeyRepo for &'a mut T
where
    T: PrivateKeyRepo,
{
    type PrivateKey = T::PrivateKey;
    type Error = T::Error;

    async fn store(&self, actor_id: Url, key: &Self::PrivateKey) -> Result<(), Self::Error> {
        T::store(self, actor_id, key).await
    }

    async fn fetch(&self, actor_id: &Url) -> Result<Self::PrivateKey, Self::Error> {
        T::fetch(self, actor_id).await
    }
}

#[async_trait::async_trait(?Send)]
impl<T> PrivateKeyRepo for Box<T>
where
    T: PrivateKeyRepo,
{
    type PrivateKey = T::PrivateKey;
    type Error = T::Error;

    async fn store(&self, actor_id: Url, key: &Self::PrivateKey) -> Result<(), Self::Error> {
        T::store(self, actor_id, key).await
    }

    async fn fetch(&self, actor_id: &Url) -> Result<Self::PrivateKey, Self::Error> {
        T::fetch(self, actor_id).await
    }
}

#[async_trait::async_trait(?Send)]
impl<T> PrivateKeyRepo for Rc<T>
where
    T: PrivateKeyRepo,
{
    type PrivateKey = T::PrivateKey;
    type Error = T::Error;

    async fn store(&self, actor_id: Url, key: &Self::PrivateKey) -> Result<(), Self::Error> {
        T::store(self, actor_id, key).await
    }

    async fn fetch(&self, actor_id: &Url) -> Result<Self::PrivateKey, Self::Error> {
        T::fetch(self, actor_id).await
    }
}

#[async_trait::async_trait(?Send)]
impl<T> PrivateKeyRepo for Arc<T>
where
    T: PrivateKeyRepo,
{
    type PrivateKey = T::PrivateKey;
    type Error = T::Error;

    async fn store(&self, actor_id: Url, key: &Self::PrivateKey) -> Result<(), Self::Error> {
        T::store(self, actor_id, key).await
    }

    async fn fetch(&self, actor_id: &Url) -> Result<Self::PrivateKey, Self::Error> {
        T::fetch(self, actor_id).await
    }
}
