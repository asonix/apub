//! An implementation of Client based on background_jobs
//! ```rust
//! use apub_core::session::SessionFactory;
//! use apub_background_jobs::{JobClient, ClientFactory, DeliverJob};
//! use apub_openssl::OpenSsl;
//! use apub_reqwest::{ReqwestClient, SignatureConfig};
//! use background_jobs::{memory_storage::Storage, WorkerConfig};
//! use openssl::{pkey::PKey, rsa::Rsa};
//! use url::Url;
//!
//! #[derive(Clone)]
//! struct State {
//!     config: SignatureConfig,
//!     client: reqwest::Client,
//! }
//!
//! impl ClientFactory for State {
//!     type Crypto = OpenSsl;
//!     type Client = ReqwestClient<OpenSsl>;
//!
//!     fn build_client(&self, crypto: &Self::Crypto) -> Self::Client {
//!         ReqwestClient::new(self.client.clone().into(), self.config.clone(), crypto.clone())
//!     }
//! }
//!
//! impl SessionFactory for State {
//!     type Session = ();
//!
//!     fn build_session(&self) -> Self::Session {
//!         ()
//!     }
//! }
//!
//! #[actix_rt::main]
//! async fn main() -> Result<(), Box<dyn std::error::Error>> {
//!     let config = SignatureConfig::default();
//!
//!     let http = reqwest::Client::new();
//!
//!     let manager = WorkerConfig::new_managed(Storage::new(), move |_| State {
//!         config: config.clone(),
//!         client: http.clone(),
//!     })
//!     .register::<DeliverJob<State, OpenSsl>>()
//!     .start();
//!
//!     let private_key = PKey::from_rsa(Rsa::generate(1024)?)?;
//!     let crypto = OpenSsl::new("key-id".to_string(), private_key);
//!
//!     let inbox: Url = "https://masto.asonix.dog/inbox".parse()?;
//!     // let activity = /* ... */;
//!     // JobClient::<State>::new(crypto, queue_handle.clone()).enqueue(inbox, &activity)?;
//!
//!     drop(manager);
//!
//!     Ok(())
//! }
//! ```

#![deny(missing_docs)]

use apub_core::{
    deliver::Deliver,
    digest::DigestFactory,
    session::{Session, SessionFactory},
    signature::PrivateKey,
};
use background_jobs::{ActixJob, QueueHandle};
use std::{future::Future, marker::PhantomData, pin::Pin};
use url::Url;

/// A trait describing acquiring an HTTP Client type from Job State and crypto
pub trait ClientFactory {
    /// The cryptography implementation associated with this client
    type Crypto;

    /// The client type produced by this factory
    type Client: Deliver;

    /// Produce the client
    fn build_client(&self, crypto: &Self::Crypto) -> Self::Client;
}

/// The actual Job type that gets queued
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DeliverJob<State, Crypto> {
    inbox: Url,
    activity: serde_json::Value,

    crypto: Crypto,

    #[serde(skip)]
    _state: PhantomData<fn(State)>,
}

/// A Deliver implementation backed by Background Jobs
pub struct JobClient<State: ClientFactory> {
    handle: QueueHandle,
    crypto: State::Crypto,
    _state: PhantomData<fn(State)>,
}

/// Errors produced when queueing jobs
#[derive(Debug)]
pub struct EnqueueError(pub anyhow::Error);

impl<State> JobClient<State>
where
    State: ClientFactory + SessionFactory + Clone + 'static,
    State::Crypto: for<'de> serde::de::Deserialize<'de>
        + serde::ser::Serialize
        + DigestFactory
        + PrivateKey
        + Clone
        + std::panic::UnwindSafe
        + 'static,
{
    /// produce a Client given Cryptography and a QueueHandle
    ///
    /// This client should be used only to deliver on behalf of the actor associated with the provided
    /// crypto. In most cases, this means the client is 1-time-use
    pub fn new(crypto: State::Crypto, handle: QueueHandle) -> Self {
        JobClient {
            handle,
            crypto,
            _state: PhantomData,
        }
    }

    /// Enqueue the activity to be delivered, consuming the client
    pub async fn enqueue<T>(self, inbox: Url, activity: &T) -> Result<(), EnqueueError>
    where
        T: serde::ser::Serialize,
    {
        queue_deliver::<T, State>(inbox, activity, self.crypto, &self.handle)
            .await
            .map_err(EnqueueError)
    }
}

impl std::fmt::Display for EnqueueError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.0, f)
    }
}

impl std::error::Error for EnqueueError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(self.0.as_ref())
    }
}

#[async_trait::async_trait(?Send)]
impl<State> Deliver for JobClient<State>
where
    State: ClientFactory + SessionFactory + Clone + 'static,
    State::Crypto: for<'de> serde::de::Deserialize<'de>
        + serde::ser::Serialize
        + DigestFactory
        + PrivateKey
        + Clone
        + std::panic::UnwindSafe
        + 'static,
{
    type Error = EnqueueError;

    async fn deliver<T: serde::ser::Serialize, S: Session>(
        &self,
        inbox: &Url,
        activity: &T,
        _session: S,
    ) -> Result<(), Self::Error> {
        queue_deliver::<T, State>(inbox.clone(), activity, self.crypto.clone(), &self.handle)
            .await
            .map_err(EnqueueError)
    }
}

async fn queue_deliver<T, State>(
    inbox: Url,
    activity: &T,
    crypto: State::Crypto,
    handle: &QueueHandle,
) -> anyhow::Result<()>
where
    T: serde::ser::Serialize,
    State: ClientFactory + SessionFactory + Clone + 'static,
    State::Crypto: for<'de> serde::de::Deserialize<'de>
        + serde::ser::Serialize
        + DigestFactory
        + PrivateKey
        + std::panic::UnwindSafe
        + 'static,
{
    let job: DeliverJob<State, State::Crypto> = DeliverJob {
        inbox,
        activity: serde_json::to_value(activity)?,
        crypto,
        _state: PhantomData,
    };

    handle.queue(job).await
}

impl<State, Crypto> ActixJob for DeliverJob<State, Crypto>
where
    State: ClientFactory<Crypto = Crypto> + SessionFactory + Clone + 'static,
    Crypto: for<'de> serde::de::Deserialize<'de>
        + serde::ser::Serialize
        + PrivateKey
        + DigestFactory
        + 'static,
{
    const NAME: &'static str = "apub-background-jobs::DeliverJob";
    type State = State;
    type Future = Pin<Box<dyn Future<Output = anyhow::Result<()>>>>;

    fn run(self, state: Self::State) -> Self::Future {
        Box::pin(async move {
            let client = state.build_client(&self.crypto);
            let session = state.build_session();
            client
                .deliver(&self.inbox, &self.activity, session)
                .await
                .map_err(|e| anyhow::anyhow!("{}", e))?;

            Ok(())
        })
    }
}
